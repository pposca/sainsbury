import unittest

from sainsbury.product_spyder import ProductSpyder


class TestProductSpyder(unittest.TestCase):
    def setUp(self):
        self.target_1 = {
            'url': "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/"
                   "2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-"
                   "loose-300g.html",
            'as_dict': {
                'description': 'Avocados',
                'size': '38.67kb',
                'title': "Sainsbury's Avocado Ripe & Ready XL Loose 300g",
                'unit_price': 1.5
            }
        }

        self.target_2 = {
            'url': "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/"
                   "2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g"
                   ".html",
            'as_dict': {
                'description': 'Apricots',
                'size': '38.27kb',
                'title': "Sainsbury's Apricot Ripe & Ready x5",
                'unit_price': 3.5
            }
        }

        self.product_1 = None
        self.product_2 = None

    def test_product_spyder_creation_without_scrape(self):
        self.product_1 = ProductSpyder(self.target_1['url'], False)
        self.assertIsInstance(self.product_1, ProductSpyder)
        self.assertEqual(self.product_1._product_url, self.target_1['url'])
        self.assertIsNone(self.product_1._title)
        self.assertIsNone(self.product_1._unit_price)
        self.assertIsNone(self.product_1._description)
        self.assertIsNone(self.product_1._size)

        self.product_2 = ProductSpyder(self.target_2['url'], False)
        self.assertIsInstance(self.product_2, ProductSpyder)
        self.assertEqual(self.product_2._product_url, self.target_2['url'])
        self.assertIsNone(self.product_2._title)
        self.assertIsNone(self.product_2._unit_price)
        self.assertIsNone(self.product_2._description)
        self.assertIsNone(self.product_2._size)

    def test_product_spyder_creation_with_scrape(self):
        self.product_1 = ProductSpyder(self.target_1['url'])
        self.assertIsInstance(self.product_1, ProductSpyder)
        self.assertEqual(self.product_1._product_url, self.target_1['url'])

        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1._title, target_1['title'])
        self.assertEqual(self.product_1._unit_price, target_1['unit_price'])
        self.assertEqual(self.product_1._description, target_1['description'])
        self.assertEqual(self.product_1._size, target_1['size'])

        self.product_2 = ProductSpyder(self.target_2['url'], True)
        self.assertIsInstance(self.product_2, ProductSpyder)
        self.assertEqual(self.product_2._product_url, self.target_2['url'])

        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2._title, target_2['title'])
        self.assertEqual(self.product_2._unit_price, target_2['unit_price'])
        self.assertEqual(self.product_2._description, target_2['description'])
        self.assertEqual(self.product_2._size, target_2['size'])

    def test_product_spyder_scrape(self):
        self.product_1 = ProductSpyder(self.target_1['url'], False)
        self.product_1.scrape()
        self.assertIsInstance(self.product_1, ProductSpyder)
        self.assertEqual(self.product_1._product_url, self.target_1['url'])

        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1._title, target_1['title'])
        self.assertEqual(self.product_1._unit_price, target_1['unit_price'])
        self.assertEqual(self.product_1._description, target_1['description'])
        self.assertEqual(self.product_1._size, target_1['size'])

        self.product_2 = ProductSpyder(self.target_2['url'], False)
        self.product_2.scrape()
        self.assertIsInstance(self.product_2, ProductSpyder)
        self.assertEqual(self.product_2._product_url, self.target_2['url'])

        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2._title, target_2['title'])
        self.assertEqual(self.product_2._unit_price, target_2['unit_price'])
        self.assertEqual(self.product_2._description, target_2['description'])
        self.assertEqual(self.product_2._size, target_2['size'])

    def test_product_spyder_get_title(self):
        self.product_1 = ProductSpyder(self.target_1['url'])
        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1.title, target_1['title'])

        self.product_2 = ProductSpyder(self.target_2['url'])
        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2.title, target_2['title'])

    def test_product_spyder_get_unit_price(self):
        self.product_1 = ProductSpyder(self.target_1['url'])
        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1.unit_price, target_1['unit_price'])

        self.product_2 = ProductSpyder(self.target_2['url'])
        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2.unit_price, target_2['unit_price'])

    def test_product_spyder_get_description(self):
        self.product_1 = ProductSpyder(self.target_1['url'])
        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1.description, target_1['description'])

        self.product_2 = ProductSpyder(self.target_2['url'])
        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2.description, target_2['description'])

    def test_product_spyder_get_size(self):
        self.product_1 = ProductSpyder(self.target_1['url'])
        target_1 = self.target_1['as_dict']
        self.assertEqual(self.product_1.size, target_1['size'])

        self.product_2 = ProductSpyder(self.target_2['url'])
        target_2 = self.target_2['as_dict']
        self.assertEqual(self.product_2.size, target_2['size'])
