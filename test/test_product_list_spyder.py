import json
import unittest

from bs4 import BeautifulSoup
from sainsbury.product_list_spyder import ProductListSpyder
from sainsbury.product_spyder import ProductSpyder


class TestProductListSpyder(unittest.TestCase):
    def setUp(self):
        self.target_1_url = "http://hiring-tests.s3-website-eu-west-1.amazon" \
                            "aws.com/2015_Developer_Scrape/5_products.html"

        self.product_list_1 = None

    def test_product_list_spyder_creation_without_scrape(self):
        self.product_list_1 = ProductListSpyder(self.target_1_url, 5)
        self.assertIsInstance(self.product_list_1, ProductListSpyder)
        self.assertIsInstance(self.product_list_1._soup, BeautifulSoup)
        self.assertEqual(self.product_list_1._product_list_url,
                         self.target_1_url)
        self.assertEqual(self.product_list_1._number_of_processes, 5)
        self.assertIsNone(self.product_list_1._products)

    def test_product_list_spyder_get_products(self):
        self.product_list_1 = ProductListSpyder(self.target_1_url, 5)
        self.assertIsNone(self.product_list_1._products)

        products = self.product_list_1.products
        self.assertIsNotNone(self.product_list_1._products)
        self.assertEqual(self.product_list_1.products,
                         self.product_list_1._products)
        for product in products:
            self.assertIsInstance(product, ProductSpyder)

    def test_product_list_spyder_as_json(self):
        self.product_list_1 = ProductListSpyder(self.target_1_url, 5)
        result = json.loads(self.product_list_1.as_json())
        self.assertIn('total', result)
        self.assertIn('results', result)
        self.assertIsInstance(result['total'], float)
        self.assertIsInstance(result['results'], list)

        for product in result['results']:
            self.assertIn('title', product)
            self.assertIsInstance(product['title'], str)

            self.assertIn('size', product)
            self.assertIsInstance(product['size'], str)

            self.assertIn('unit_price', product)
            self.assertIsInstance(product['unit_price'], float)

            self.assertIn('description', product)
            self.assertIsInstance(product['description'], str)
