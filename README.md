# Sainsbury’s Software Engineering Test

This repository contains a package to solve in Python the Software Engineering
Test that Sainsbury uses as a part of the evaluation process to hire new
developers.

## Overview

Due its simplicity, there is a very *quick and dirty* functional way to solve
the exercise, using only a couple of functions. Despite this, I have decided to
use the Object Oriented paradigm because usually is better to design and
maintain solutions for complex problems that require medium and big size
architectures with a lot of entities communicating among them.

The first approach I made was monoprocess, but as it felt a bit slow, I did a
little of refactoring to allow multiprocessing, what it quite easy with Python.
Scraping is the typical blocking problem where multiprocessing or async
programing are game changer. If you use a single thread, it blocks on every
Internet request, and the overall delay is the sum of each delay. Both
multiprocessing and async programming (coroutines, for example) reduce this
overall delay. I have chosen multiprocessing just because in Python is older
and more backwards compatible, whereas async programming is quite new
(I mean as a part of the standard library) and I don't master it yet.

## Python version

I have used Python 3.5.1, but it should work in any version of Python 3

## Dependencies

The solution has only 2 dependencies that are well known, tested and widely used
libraries:

    - BeautifulSoup
    - Requests

There is a *requirements.txt* file obtained using *pip freeze*. It can be used
to reproduce exactly my virtualenv.

## Program and tests execution

First, the best is create a virtualenv and install the dependencies:

```bash
cd sainsbury
pyvenv ENV
source ENV/bin/activate
pip install beautifulsoup4
pip install requests
```

After this, just do:

```bash
python scraping.py
```

To execute the test, you can do:

```bash
python -m unittest discover -v
```

Or execute each text separately:

```bash
python -m unittest test.test_product_list_spyder -v
python -m unittest test.test_product_spyder -v
```

Keep in mind you need Internet conextion to execute the tests.