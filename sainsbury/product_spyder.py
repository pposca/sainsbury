import re

import requests
from bs4 import BeautifulSoup


class ProductSpyder:
    """
    Scrapes a product from its url.

    Once scraped the product is suppose to be read-only. It exposes, then,  a
    read-only interface to get the properties of the product, and a method to
    retrieve them as a dictionary, in order to easily convert it to json.
    """

    def __init__(self, product_url, scrape=True):
        """
        Creates a new product and optionally scrapes it from Internet

        :param product_url: The url the product has to be scraped from.
        :param scrape: Boolean to denote if information should be scraped
        directly. Useful for multiprocessing.
        """
        self._product_url = product_url
        self._title = None
        self._unit_price = None
        self._description = None
        self._size = None

        if scrape:
            self.scrape()

    def scrape(self):
        """
        Scrapes the product information from Internet
        """
        r = requests.get(self._product_url)
        soup = BeautifulSoup(r.text, 'html.parser')

        # Title
        self._title = soup.select(
            '.productTitleDescriptionContainer > h1')[0].text.strip()
        # Unit price
        self._unit_price = float(re.findall("\d+\.\d+", soup.select(
            '.pricePerUnit')[0].text)[0])
        # Description
        self._description = soup.select('.productText')[0].text.strip()
        # Size
        bytes = float(r.headers['content-length'])
        self._size = '{:.2f}kb'.format(bytes / 1024)

    @property
    def title(self):
        """
        :return: The title of the product
        """
        return self._title

    @property
    def size(self):
        """
        :return: The size in kb of the linked HTML
        """
        return self._size

    @property
    def unit_price(self):
        """
        :return: The unit price of the product in pounds
        """
        return self._unit_price

    @property
    def description(self):
        """
        :return: The description of the product
        """
        return self._description

    def as_dict(self):
        """
        :return: A dict representation of the product. Used to easily convert
        it to json
        """
        return {
            'title': self.title,
            'size': self.size,
            'unit_price': self.unit_price,
            'description': self.description,
        }
