import json
from multiprocessing.pool import Pool

import requests
from bs4 import BeautifulSoup
from . import product_spyder


class ProductListSpyder:
    """
    Scrapes a list of products from its url.

    Once scraped, the list can be accessed directly and it is intended to be
    read-only. It exposes, in addition to the list, a method 'as_json' that
    retrieves the json representation of the list.
    
    As multiple request to Internet a performed (blocking I/O operations), a
    pool of processes is used in order to reduce the overall delay.
    """

    def __init__(self, product_list_url, number_of_processes=1):
        """
        Creates a new list of products. Products are not scraped until they are
        requested using the 'products' property or the 'as_json' method.

        :param product_list_url: The url the list of products has to be scraped
        from.
        :param number_of_processes: number of processes used to scrape the list
        of products.
        """
        self._product_list_url = product_list_url
        self._products = None
        self._number_of_processes = number_of_processes

        r = requests.get(product_list_url)
        self._soup = BeautifulSoup(r.text, 'html.parser')

    @property
    def products(self):
        """
        Scrapes the products of the list if needed and return them.

        :return: The list of products
        """
        if self._products is None:
            self._products = []

            product_links = self._soup.select('.productInfo a')

            product_links = [link['href'] for link in product_links]

            with Pool(self._number_of_processes) as p:
                self._products = p.map(product_spyder.ProductSpyder,
                                       product_links)

        return self._products

    def as_json(self, pretty=True):
        """
        :return: A json pretty representation of the list of product.
        """
        output = {
            'results': [],
            'total': 0
        }
        for product in self.products:
            output['results'].append(product.as_dict())
            output['total'] += product.unit_price

        output['total'] = round(output['total'], 3)

        if pretty:
            return json.dumps(output, indent=4, sort_keys=True)
        else:
            return json.dumps(output)
