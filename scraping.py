from sainsbury.product_list_spyder import ProductListSpyder

if __name__ == '__main__':

    TARGET_URL = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/' \
                 '2015_Developer_Scrape/5_products.html'
    PROCESSES = 5

    products = ProductListSpyder(TARGET_URL, PROCESSES)

    print(products.as_json())
